let priceOutput = document.getElementById('price-output'),
    priceInput = document.getElementById('priceInput'),
    priceInputWrap = document.querySelector('.input-wrap'),
    errorMessage = document.getElementById('error-message');

createPriceValueBlock = () => {
    const priceValueBlock = document.createElement('div');
    const priceValueSpan = document.createElement('span');
    const xButton = document.createElement('button');
    xButton.addEventListener('click', () => {
        priceValueBlock.remove();
        priceInput.value = "";
});
    priceValueSpan.innerText = `Current price is ${priceInput.value}`;
    xButton.innerText = "x";
    priceValueBlock.appendChild(priceValueSpan);
    priceValueBlock.appendChild(xButton);
    priceValueBlock.classList.add('price-value-style');
    xButton.classList.add('btn-style');
    document.body.insertBefore(priceValueBlock, priceInputWrap);
    priceOutput.appendChild(priceValueBlock);
};

priceInput.addEventListener('focus', () => {
    priceInput.classList.add('price-input');
});

priceInput.addEventListener('blur', () => {
    priceInput.classList.remove('price-input');
    if (Number(priceInput.value < 0)){
        errorMessage.innerText = 'Please enter correct value';
        errorMessage.style.color = 'red';
        priceInput.style.color = 'black';
        priceOutput.innerText = "";
    }else{
        createPriceValueBlock();
        priceInput.style.color = 'green';
        errorMessage.innerText = "";
    }
});

